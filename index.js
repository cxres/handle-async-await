/*!
 *  Copyright (c) 2021, Rahul Gupta
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  SPDX-License-Identifier: MPL-2.0
 */
async function handleAsyncAwait(promise, errorHandler) {
  try {
    return await promise;
  }
  catch (error) {
    if (typeof errorHandler === 'string') {
      if (error.message) {
        error.message = `${errorHandler}${errorHandler.match(/\s$/) ? '' : ' '}${error.message}`;
      }
      else {
        error.message = errorHandler;
      }
      throw error;
    }
    if (typeof errorHandler === 'function') {
      return errorHandler(error);
    }
    throw error;
  }
}

export default handleAsyncAwait;
